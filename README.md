This is a [Hugo module](https://gohugo.io/hugo-modules/) that packages the
[Bootstrap Icons](https://getbootstrap.com/) ready to be used in Hugo.

## Use

Add the component to your Hugo site's config:

```yaml
module:
  imports:
    - path: "gitlab.com/writeonlyhugo/hugo-module-bootstrap-icons"
```

Now you can use the partial `bs-icon.html` to insert the icons anywhere:

```go-html-template
{{ partial "bs-icon.html" "clock" }}
```

If you want to use the font and classes, you need something like this:

```go-html-template
{{- $bsi := resources.Get "bootstrap-icons/font/bootstrap-icons.scss" -}}
{{- $bsi := $bsi | resources.ToCSS (dict "outputStyle" "compressed" "includePaths" (slice "assets")) -}}
<link rel="stylesheet" href="{{ $bsi.Permalink }}" type="text/css" media="all">

{{- $bsifont := resources.Match "bootstrap-icons/font/fonts/*" -}}
{{ range $bsifont }} {{- .Publish -}} {{ end }}
```
